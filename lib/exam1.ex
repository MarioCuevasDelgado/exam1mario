defmodule Exam1 do
  def median(l) do
    acomodar = Enum.sort(l)

    if(rem(length(l), 2) == 1) do Enum.at(acomodar, div(length(l), 2))

    else
      (Enum.at(acomodar, div(length(acomodar), 2)) + Enum.at(acomodar, div(length(acomodar), 2) - 1)) / 2
    end

  end

  def quartile1(l) do
    acomodar = Enum.sort(l)

    if(rem(length(l), 2) == 1) do Enum.take(acomodar, div(length(acomodar)+1, 2))
    median(Enum.take(acomodar, div(length(acomodar)+1, 2)))

    else
      median(Enum.take(acomodar, div(length(acomodar), 2)))
    end

  end

  def quartile3(l) do
    acomodar = Enum.sort(l)

    if(rem(length(l), 2) == 1) do Enum.take(acomodar, div(length(acomodar), 2))
    median(Enum.take(acomodar, -1*div(length(acomodar)+1, 2)))

    else
      median(Enum.take(acomodar,-1*div(length(acomodar), 2)))
    end

  end

  def iqr(l) do
      quartile3(l)+(-1*quartile1(l))
  end

end
